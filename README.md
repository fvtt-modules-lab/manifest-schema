# FoundryVTT Manifest JSON Schema

This repository contains JSON Schema definition files for Foundry VTT manifests
(`module.json` and `system.json`). Most of the descriptions are taken directly
from the foundry documentation. JSON Schema can help you with validation and
autocomplete when creating your manifest.

- https://foundryvtt.com/article/module-development/
- https://foundryvtt.com/article/system-development/

## Module schema URL

```
https://gitlab.com/fvtt-modules-lab/manifest-schema/-/raw/master/moduleSchema.json
```

## System schema URL

```
https://gitlab.com/fvtt-modules-lab/manifest-schema/-/raw/master/systemSchema.json
```

## Usage in editors

Many editors have support for JSON Schema validation and autocomplete. If your
editor automatically supports JSON Schema out of the box, you can add the
`$schema` property to the top of your `module.json` or `system.json` file
(ensure you use the correct URL from above).

```json
{
  "$schema": "https://gitlab.com/fvtt-modules-lab/manifest-schema/-/raw/master/moduleSchema.json",
  "name": "..."
}
```

## Automatic use in VSCode

If you use VSCode, you can add the following to your `settings.json` file to
automatically verify:

```json
  "json.schemas": [
    {
      "fileMatch": ["module.json"],
      "url": "https://gitlab.com/fvtt-modules-lab/manifest-schema/-/raw/master/moduleSchema.json"
    },
    {
      "fileMatch": ["system.json"],
      "url": "https://gitlab.com/fvtt-modules-lab/manifest-schema/-/raw/master/systemSchema.json"
    }
  ]
```

## Issues

This schema was compiled by the community and may contain inaccuracies or
outdated information. Feel free to write an issue or pull request if you find
any.

Some properties may be required, but are not tagged as such, and some propertes
may have to adhere to specific patterns which are not verified (although others
are).
